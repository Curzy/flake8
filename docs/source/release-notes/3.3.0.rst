3.3.0 -- 2016-xx-yy
-------------------

You can view the `3.3.0 milestone`_ on GitLab for more details.

- Update upper bound on PyFlakes to allow for PyFlakes 1.5.0 (`GitLab#290`_)

- Add methods to Report class that will be called when Flake8 starts and
  finishes processing a file. (`GitLab#251`_)

- Dramatically improve the performance of Flake8 (`GitLab!156`_)

- Display the local file path instead of the temporary file path when
  using the git hook (`GitLab#244`_)

- Fix problem where hooks should only check \*.py files. (See also
  `GitLab#268`_)

- Added unique error codes for all missing PyFlakes messages (14 new
  codes, see all :ref:`Error / Violation Codes <error_codes>`)

- Force ``flake8 --version`` to be repeatable between invocations. (See also
  `GitLab#297`_)

.. all links
.. _3.3.0 milestone:
    https://gitlab.com/pycqa/flake8/milestones/16

.. issue links
.. _GitLab#244:
    https://gitlab.com/pycqa/flake8/issues/244
.. _GitLab#251:
    https://gitlab.com/pycqa/flake8/issues/251
.. _GitLab#268:
    https://gitlab.com/pycqa/flake8/issues/268
.. _GitLab#290:
    https://gitlab.com/pycqa/flake8/issues/290
.. _GitLab#297:
    https://gitlab.com/pycqa/flake8/issues/297

.. merge request links
.. _GitLab!156:
    https://gitlab.com/pycqa/flake8/merge_requests/156
